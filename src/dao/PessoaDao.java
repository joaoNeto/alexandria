package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import model.Pessoa;

public class PessoaDao extends Crud<Pessoa>{

    public PessoaDao() {
        super("pessoa");
    }

    @Override
    public Boolean cadastrar(Pessoa obj) {
        String sql = "insert into "+super.getTabela()+"(email,nome,telefone,cpf,bairro,endereco,numEndereco,senha,id_tipo_pessoa) values("
                   + "'"+obj.getEmail()+"',"
                   + "'"+obj.getNome()+"',"
                   + "'"+obj.getTelefone()+"',"
                   + "'"+obj.getCpf()+"',"
                   + "'"+obj.getBairro()+"',"                
                   + "'"+obj.getEndereco()+"',"                
                   + ""+obj.getNumEndereco()+","                
                   + "'"+obj.getSenha()+"',"                
                   + obj.getIdTipoPessoa()+""                
                   + ")";
        return super.executaSqlReturnBool(sql);
    }

    @Override
    public ArrayList<Pessoa> buscarTodos() {
        ArrayList arrRetorno = new ArrayList();
        String sql = "select * from "+super.getTabela();
        ResultSet rs = super.executaSqlReturnResultSet(sql);
        try{
            rs.first();
            do{
                arrRetorno.add(new Pessoa(rs.getString("email"),
                                          rs.getString("nome"),
                                          rs.getString("telefone"),
                                          rs.getString("cpf"),
                                          rs.getString("bairro"),
                                          rs.getString("endereco"),
                                          rs.getInt("numEndereco"),
                                          rs.getString("senha"),
                                          rs.getInt("id_tipo_pessoa")));
            }while(rs.next());
        }catch(Exception ex){
            
        }
        return arrRetorno;        
    }

    @Override
    public Pessoa buscarUm(int id) {
        Pessoa obj = new Pessoa();
        String sql = "select * from "+super.getTabela()+" where id = "+id;
        ResultSet rs = super.executaSqlReturnResultSet(sql);
        try{
            rs.first();
            obj.setEmail(rs.getString("email"));
            obj.setNome(rs.getString("nome"));
            obj.setTelefone(rs.getString("telefone"));
            obj.setCpf(rs.getString("cpf"));
            obj.setBairro(rs.getString("bairro"));                
            obj.setEndereco(rs.getString("endereco"));                
            obj.setNumEndereco(rs.getInt("numEndereco"));             
            obj.setSenha(rs.getString("senha"));                
            obj.setIdTipoPessoa(rs.getInt("id_tipo_pessoa"));             
            
        }catch(Exception ex){
            
        }
        return obj;        
    }

    public Pessoa buscarUm(String email, String senha){
        Pessoa obj = new Pessoa();
        String sql = "select * from "+super.getTabela()+" where email = '"+email+"' and senha = '"+senha+"' ";
        ResultSet rs = super.executaSqlReturnResultSet(sql);
        try{
            rs.first();
            obj.setEmail(rs.getString("email"));
            obj.setNome(rs.getString("nome"));
            obj.setTelefone(rs.getString("telefone"));
            obj.setCpf(rs.getString("cpf"));
            obj.setBairro(rs.getString("bairro"));                
            obj.setEndereco(rs.getString("endereco"));                
            obj.setNumEndereco(rs.getInt("numEndereco"));             
            obj.setSenha(rs.getString("senha"));                
            obj.setIdTipoPessoa(rs.getInt("id_tipo_pessoa"));             
        }catch(Exception ex){
            obj = null;
        }
        return obj;            
    }
    
    @Override
    public Boolean atualizar(Pessoa obj, int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Boolean atualizar(Pessoa obj, String email) {
        String sql = "update "+super.getTabela()+" set "
                   + "email     = '"+obj.getNome()+"', "
                   + "nome      = '"+obj.getEmail()+"', "
                   + "telefone  = '"+obj.getTelefone()+"', "
                   + "cpf       = '"+obj.getCpf()+"', "
                   + "bairro    = '"+obj.getBairro()+"', "
                   + "endereco  = '"+obj.getEndereco()+"', "
                   + "numEndereco = '"+obj.getNumEndereco()+"', "
                   + "senha     = '"+obj.getSenha()+"', "
                   + "id_tipo_pessoa = '"+obj.getIdTipoPessoa()+"'"
                   + "where email = '"+email+"'";
        return super.executaSqlReturnBool(sql);
    }
    
    
    @Override
    public Boolean excluir(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public Boolean excluir(String email) {
        String sql = "delete from "+super.getTabela()+" where email = '"+email+"'";
        return super.executaSqlReturnBool(sql);
    }
    
    
}
