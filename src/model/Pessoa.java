package model;

public class Pessoa {

    private String email;
    private String nome;
    private String telefone;
    private String cpf;
    private String bairro;
    private String endereco;
    private int numEndereco;
    private String senha;
    private int idTipoPessoa;

    public Pessoa() {
    }

    public Pessoa(String email, String nome, String telefone, String cpf, String bairro, String endereco, int numEndereco, String senha, int id_tipo_pessoa) {
        this.email = email;
        this.nome = nome;
        this.telefone = telefone;
        this.cpf = cpf;
        this.bairro = bairro;
        this.endereco = endereco;
        this.numEndereco = numEndereco;
        this.senha = senha;
        this.idTipoPessoa = id_tipo_pessoa;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public int getNumEndereco() {
        return numEndereco;
    }

    public void setNumEndereco(int numEndereco) {
        this.numEndereco = numEndereco;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public int getIdTipoPessoa() {
        return idTipoPessoa;
    }

    public void setIdTipoPessoa(int id_tipo_pessoa) {
        this.idTipoPessoa = id_tipo_pessoa;
    }
    
    
}
