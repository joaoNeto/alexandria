package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import model.Acl;

public final class AclDao extends Crud<Acl> {
    
    public AclDao() {
        super("acl"); // nome da tabela
    }

    @Override
    public Boolean cadastrar(Acl obj) {
        String sql = "insert into "+super.getTabela()+"(nome) values('"+obj.getNome()+"')";
        return super.executaSqlReturnBool(sql);
    }

    @Override
    public ArrayList<Acl> buscarTodos() {
        ArrayList arrRetorno = new ArrayList();
        String sql = "select * from "+super.getTabela();
        ResultSet rs = super.executaSqlReturnResultSet(sql);
        try{
            rs.first();
            do{
                arrRetorno.add(new Acl(rs.getInt("id"),rs.getString("nome")));
            }while(rs.next());
        }catch(Exception ex){
            
        }
        return arrRetorno;
    }

    @Override
    public Acl buscarUm(int id) {
        Acl retorno = new Acl();
        String sql = "select * from "+super.getTabela()+" where id = "+id;
        ResultSet rs = super.executaSqlReturnResultSet(sql);
        try{
            rs.first();
            retorno.setNome(rs.getString("nome")); 
        }catch(Exception ex){
            
        }
        return retorno;
    }

    @Override
    public Boolean atualizar(Acl obj, int id) {
        String sql = "update "+super.getTabela()+" set nome = '"+obj.getNome()+"' where id = "+id;
        return super.executaSqlReturnBool(sql);
    }

    @Override
    public Boolean excluir(int id) {
        String sql = "delete from "+super.getTabela()+" where id = "+id;
        return super.executaSqlReturnBool(sql);
    }

}
