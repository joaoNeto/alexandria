package dao;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import model.EmprestimoLivro;

public final class EmprestimoLivroDao extends Crud<EmprestimoLivro>{

    public EmprestimoLivroDao() {
        super("emprestimo_livro"); // tabela do banco
    }

    @Override
    public Boolean cadastrar(EmprestimoLivro obj) {
         
        String dataAtual     = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String dataDevolucao = new SimpleDateFormat("yyyy-MM-dd").format(obj.getDtDevolucao());
        
        String sql = "insert into "+super.getTabela()+"(id_pessoa, id_livro, dt_devolucao, dt_emprestimo) values("
                   + obj.getIdPessoa()+", "
                   + obj.getIdLivro()+", "
                   + "'"+dataDevolucao+"', "
                   + "'"+dataAtual+"')";
        System.out.println(sql);
        return super.executaSqlReturnBool(sql);
    }

    @Override
    public ArrayList<EmprestimoLivro> buscarTodos() {
        ArrayList arrRetorno = new ArrayList();
        String sql = "select * from "+super.getTabela();
        ResultSet rs = super.executaSqlReturnResultSet(sql);
        try{
            rs.first();
            do{
                arrRetorno.add(new EmprestimoLivro(rs.getInt("id_pessoa"),
                                                   rs.getInt("id_livro"),
                                                   rs.getDate("dt_devolucao"),
                                                   rs.getDate("dt_emprestimo")));
            }while(rs.next());
        }catch(Exception ex){
            
        }
        return arrRetorno;
    }
    
    public ArrayList<EmprestimoLivro> buscarTodos(int idPessoa) {
        ArrayList arrRetorno = new ArrayList();
        String sql = "select * from "+super.getTabela()+" where id_pessoa = "+idPessoa;
        ResultSet rs = super.executaSqlReturnResultSet(sql);
        try{
            rs.first();
            do{
                arrRetorno.add(new EmprestimoLivro(rs.getInt("id_pessoa"),
                                                   rs.getInt("id_livro"),
                                                   rs.getDate("dt_devolucao"),
                                                   rs.getDate("dt_emprestimo")));
            }while(rs.next());
        }catch(Exception ex){
            
        }
        return arrRetorno;
    }    

    @Override
    public EmprestimoLivro buscarUm(int idLivro) {
        EmprestimoLivro retorno = new EmprestimoLivro();
        String sql = "select * from "+super.getTabela()+" where id_livro = "+idLivro;
        ResultSet rs = super.executaSqlReturnResultSet(sql);
        try{
            rs.first();
            retorno.setIdLivro(rs.getInt("id_livro"));
            retorno.setIdPessoa(rs.getInt("id_pessoa"));
            retorno.setDtEmprestimo(rs.getDate("dt_emprestimo"));
            retorno.setDtDevolucao(rs.getDate("dt_devolucao"));            
        }catch(Exception ex){
            
        }
        return retorno;    
    }

    public EmprestimoLivro buscarUm(int idPessoa, int idLivro) {
        EmprestimoLivro retorno = new EmprestimoLivro();
        String sql = "select * from "+super.getTabela()+" where id_livro = "+idLivro+" and id_pessoa = "+idPessoa;
        ResultSet rs = super.executaSqlReturnResultSet(sql);
        try{
            rs.first();
            retorno.setIdLivro(rs.getInt("id_livro"));
            retorno.setIdPessoa(rs.getInt("id_pessoa"));
            retorno.setDtEmprestimo(rs.getDate("dt_emprestimo"));
            retorno.setDtDevolucao(rs.getDate("dt_devolucao"));            
        }catch(Exception ex){
            
        }
        return retorno;    
    }
        
    @Override
    public Boolean atualizar(EmprestimoLivro obj, int idLivro) {
        String dataDevolucao = new SimpleDateFormat("yyyy-MM-dd").format(obj.getDtDevolucao());        
        String sql = "update "+super.getTabela()+" set dt_devolucao = "+dataDevolucao+" where id_livro = "+idLivro;
        return super.executaSqlReturnBool(sql);
    }

    @Override
    public Boolean excluir(int idLivro) {
        String sql = "delete from "+super.getTabela()+" where id_livro = "+idLivro;
        return super.executaSqlReturnBool(sql);
    }

    public Boolean excluir(int idPessoa, int idLivro) {
        String sql = "delete from "+super.getTabela()+" where id_livro = "+idLivro+" and id_pessoa = "+idPessoa;
        return super.executaSqlReturnBool(sql);
    }
    
}
