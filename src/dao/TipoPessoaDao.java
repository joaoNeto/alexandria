package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import model.TipoPessoa;

public final class TipoPessoaDao extends Crud<TipoPessoa>{

    public TipoPessoaDao() {
        super("tipo_pessoa"); // nome da tabela
    }

    @Override
    public Boolean cadastrar(TipoPessoa obj) {
        String sql = "insert into "+super.getTabela()+"(nome) values('"+obj.getNome()+"')";
        return super.executaSqlReturnBool(sql);
    }

    @Override
    public ArrayList<TipoPessoa> buscarTodos() {
        ArrayList arrRetorno = new ArrayList();
        String sql = "select * from "+super.getTabela();
        ResultSet rs = super.executaSqlReturnResultSet(sql);
        try{
            rs.first();
            do{
                arrRetorno.add(new TipoPessoa(rs.getInt("id"),rs.getString("nome")));
            }while(rs.next());
        }catch(Exception ex){
            
        }
        return arrRetorno;
    }

    @Override
    public TipoPessoa buscarUm(int id) {
        TipoPessoa retorno = new TipoPessoa();
        String sql = "select * from "+super.getTabela()+" where id = "+id;
        ResultSet rs = super.executaSqlReturnResultSet(sql);
        try{
            rs.first();
            retorno.setNome(rs.getString("nome")); 
        }catch(Exception ex){
            
        }
        return retorno;
    }

    @Override
    public Boolean atualizar(TipoPessoa obj, int id) {
        String sql = "update "+super.getTabela()+" set nome = '"+obj.getNome()+"' where id = "+id;
        return super.executaSqlReturnBool(sql);
    }

    @Override
    public Boolean excluir(int id) {
        String sql = "delete from "+super.getTabela()+" where id = "+id;
        return super.executaSqlReturnBool(sql);
    }
    
    
}
