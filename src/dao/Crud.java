package dao;

import java.sql.PreparedStatement;
import java.util.ArrayList;

public abstract class Crud<ObjTG> extends Conn{

    private String tabela;
    
    public Crud(String tabela) {
        super();
        this.tabela = tabela;
    }

    public abstract Boolean cadastrar(ObjTG obj);
    public abstract ArrayList<ObjTG> buscarTodos();
    public abstract ObjTG buscarUm(int id);
    public abstract Boolean atualizar(ObjTG obj, int id);
    public abstract Boolean excluir(int id);

    public String getTabela() {
        return tabela;
    }

    public void setTabela(String tabela) {
        this.tabela = tabela;
    }    
    
}