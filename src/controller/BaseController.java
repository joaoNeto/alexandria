package controller;

import java.util.ArrayList;
import java.util.Map;
import model.Pessoa;
import model.controller.ValidaCampo;
import java.security.*;
import java.math.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class BaseController {
    
    protected Pessoa usuarioLogado;
        
    public abstract Map index();
    
    public Pessoa getUsuarioLogado(){
        if(this.usuarioLogado != null){
            return this.usuarioLogado;
        }
        usuarioLogado = new Pessoa();
        return usuarioLogado;
    }
    
    /* ---------------------------------------------------------------------- */
    /* -- INICIO FUNCOES VALIDA CAMPOS -------------------------------------- */
    /* ---------------------------------------------------------------------- */

    public Boolean validaCampo(ValidaCampo campo){        
        Boolean retorno   = true;
        String campoTexto = campo.getText();
        
        switch (campo.getTipoCampo()){
            case texto:
                if(campoTexto.contains("'") || campoTexto.contains("--"))
                    retorno = false;
                break;
            case email:
                if(campoTexto.contains("'") || campoTexto.contains("--"))
                    retorno = false;
                break;
        }
        
        return retorno;
    }
    
    public Boolean validaCampos(ArrayList<ValidaCampo> arrCampos){
        for(ValidaCampo campo : arrCampos)
         if(!this.validaCampo(campo))
             return false;
        
        return true;
    }
    
    /* ---------------------------------------------------------------------- */
    /* -- FIM FUNCOES VALIDA CAMPOS -------------------------------------- */
    /* ---------------------------------------------------------------------- */

    public String md5(String campo){
        try {
           MessageDigest m = MessageDigest.getInstance("MD5");
           m.update(campo.getBytes(), 0, campo.length());
           return new BigInteger(1,m.digest()).toString(16);
        } catch (NoSuchAlgorithmException ex) {

        }
        return null;
    }
    
    
}
