package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import model.Livro;

public class LivroDao extends Crud<Livro>{

    public LivroDao() {
        super("livro"); // tabela do banco
    }
    
    @Override
    public Boolean cadastrar(Livro obj) {
        String sql = "insert into "+super.getTabela()+"(cod, titulo, autor, id_genero) values("
                   +obj.getCod()+", "
                   + "'"+obj.getTitulo()+"', "
                   + "'"+obj.getAutor()+"', "
                   +obj.getIdGenero()+""
                   + ")";
        return super.executaSqlReturnBool(sql);
    }

    @Override
    public ArrayList<Livro> buscarTodos() {
        ArrayList arrRetorno = new ArrayList();
        String sql = "select * from "+super.getTabela();
        ResultSet rs = super.executaSqlReturnResultSet(sql);
        try{
            rs.first();
            do{
                arrRetorno.add(new Livro(rs.getInt("cod"),
                                         rs.getString("titulo"),
                                         rs.getString("autor"),
                                         rs.getInt("idGenero")));
            }while(rs.next());
        }catch(Exception ex){
            
        }
        return arrRetorno;    
    }

    @Override
    public Livro buscarUm(int cod) {
        Livro retorno = new Livro();
        String sql = "select * from "+super.getTabela()+" where cod = "+cod;
        ResultSet rs = super.executaSqlReturnResultSet(sql);
        try{
            rs.first();
            retorno.setCod(rs.getInt("cod"));
            retorno.setTitulo(rs.getString("titulo"));
            retorno.setAutor(rs.getString("autor"));
            retorno.setIdGenero(rs.getInt("idGenero"));
        }catch(Exception ex){
            
        }
        return retorno;
    }

    @Override
    public Boolean atualizar(Livro obj, int cod) {
        String sql = "update "+super.getTabela()+" set "
                   + "titulo     = '"+obj.getTitulo()+"', "
                   + "autor      = '"+obj.getAutor()+"', "
                   + "id_genero  = "+obj.getIdGenero()+""
                   + " where cod = "+cod;
        return super.executaSqlReturnBool(sql);    }

    @Override
    public Boolean excluir(int cod) {
        String sql = "delete from "+super.getTabela()+" where cod = "+cod;
        return super.executaSqlReturnBool(sql);
    }
    
}
