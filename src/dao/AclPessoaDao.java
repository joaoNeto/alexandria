package dao;

import java.util.ArrayList;
import model.AclPessoa;

public class AclPessoaDao extends Crud<AclPessoa>{

    public AclPessoaDao() {
        super("acl_pessoa");
    }

    @Override
    public Boolean cadastrar(AclPessoa obj) {
        String sql = "insert into "+super.getTabela()+"(id_tipo_pessoa, id_acl, registrar, visualisar, alterar, deletar) values("
                +obj.getIdTipoPessoa()+", "
                +obj.getIdAcl()+", "
                +obj.getRegistrar()+", "
                +obj.getVisualisar()+", "
                +obj.getAlterar()+", "
                +obj.getDeletar()+""
                + ")";
        return super.executaSqlReturnBool(sql);
    }

    @Override
    public ArrayList<AclPessoa> buscarTodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AclPessoa buscarUm(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean atualizar(AclPessoa obj, int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean excluir(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Boolean excluir(int idAcl, int idPessoa) {
        String sql = "delete from "+super.getTabela()+" where id_tipo_pessoa = "+idPessoa+" and id_acl = "+idAcl;
        return super.executaSqlReturnBool(sql);
    }
    
    
}
