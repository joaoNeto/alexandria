package controller;

import dao.TipoPessoaDao;
import java.util.HashMap;
import java.util.Map;
import model.Pessoa;
import model.TipoPessoa;

public final class DashboardController extends BaseController{

    public DashboardController(Pessoa usuario){
        super.usuarioLogado = usuario;        
    }
        
    @Override
    public Map<String, String> index() {
        Map<String, String> retorno = new HashMap<String, String>();
        TipoPessoa tipoPessoa = new TipoPessoaDao().buscarUm(super.usuarioLogado.getIdTipoPessoa());
        retorno.put("nomeUsuario", super.usuarioLogado.getNome());
        retorno.put("tipoUsuario", tipoPessoa.getNome());
        
        return retorno;
    }
    
}
