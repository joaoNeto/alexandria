package model;

public class Livro {

    private int cod;
    private String titulo;
    private String autor;
    private int idGenero;

    public Livro(int cod, String titulo, String autor, int idGenero) {
        this.cod = cod;
        this.titulo = titulo;
        this.autor = autor;
        this.idGenero = idGenero;
    }

    public Livro() {
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getIdGenero() {
        return idGenero;
    }

    public void setIdGenero(int idGenero) {
        this.idGenero = idGenero;
    }

    
}
