package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class Conn {

    public Statement stm;
    public ResultSet rs;
    private String driver  = "com.mysql.jdbc.Driver";
    private String caminho = "jdbc:mysql://localhost:3306/alexandria";
    private String usuario = "root";
    private String senha   = "";
    public Connection conn;
    
    public Conn(){
        this.conexao();
    }
    
    public void conexao(){
        try{
            System.setProperty("jdbc.Drivers",driver);
            conn = DriverManager.getConnection(caminho, usuario, senha);
            //PreparedStatement pst = this.conn.prepareStatement("");            
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "Erro ao se connectar com o banco: \n"+ex.getMessage());
        }
    }
    
    public void desconecta(){
        try{
            conn.close();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "Erro ao se desconnectar do banco: \n"+ex.getMessage());            
        }
        
    }
    
    public PreparedStatement executaSql(String sql){  
        PreparedStatement retorno = null;
        try{
            retorno = this.conn.prepareStatement(sql);
            retorno.execute();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "Erro ao executar sql \n "+ex.getMessage());
        }
        return retorno;
    }
            
    public Boolean executaSqlReturnBool(String sql){  
        PreparedStatement retorno = null;
        Boolean r = false;
        try{
            retorno = this.conn.prepareStatement(sql);
            
            r = (retorno.executeUpdate() == 1);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "Erro ao executar sql \n "+ex.getMessage());
        }
        return r;
    }
    
    public ResultSet executaSqlReturnResultSet(String sql){
        ResultSet listaRetorno = null;
        try{
            PreparedStatement retorno = this.conn.prepareStatement(sql);
            listaRetorno = retorno.executeQuery();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "Erro ao executar sql \n "+ex.getMessage());
        }
        return listaRetorno;    
    }

    
}
