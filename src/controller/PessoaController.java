package controller;

import dao.TipoPessoaDao;
import java.util.HashMap;
import java.util.Map;
import model.Pessoa;
import model.controller.EnumEstado;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import model.TipoPessoa;

public class PessoaController extends BaseController{

    Pessoa pessoa;
    
    public PessoaController(Pessoa pessoa){
        this.pessoa = pessoa;
    }
    
    @Override
    public Map index() {
        ArrayList listaEstado       = new ArrayList();
        ArrayList listaTipoPessoa   = new ArrayList();
        Map mapaRetorno             = new HashMap();
        TipoPessoaDao tipoPessoaDao = new TipoPessoaDao();

        for (EnumEstado dir : EnumEstado.values())
            listaEstado.add(dir);
        
        mapaRetorno.put("listaEstado", listaEstado);
        
        // lista de tipos de pessoas
        ArrayList<TipoPessoa> arrTipoPessoa = tipoPessoaDao.buscarTodos();
        
        for(TipoPessoa tipoPessoa : arrTipoPessoa)
            listaTipoPessoa.add(tipoPessoa.getNome());
            
        mapaRetorno.put("listaTipoPessoa", listaTipoPessoa);
        
        if(this.pessoa != null){
            // configuracoes de edicao
            
            // buscar os dados da pessoa
        }
        
        
        
        return mapaRetorno;
    }

}
