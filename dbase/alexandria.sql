-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Máquina: 127.0.0.1
-- Data de Criação: 26-Abr-2018 às 19:17
-- Versão do servidor: 5.5.32
-- versão do PHP: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `alexandria`
--
CREATE DATABASE IF NOT EXISTS `alexandria` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `alexandria`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `acl`
--

CREATE TABLE IF NOT EXISTS `acl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `acl`
--

INSERT INTO `acl` (`id`, `nome`) VALUES
(1, 'pessoa'),
(2, 'livro');

-- --------------------------------------------------------

--
-- Estrutura da tabela `acl_pessoa`
--

CREATE TABLE IF NOT EXISTS `acl_pessoa` (
  `id_tipo_pessoa` int(2) NOT NULL,
  `id_acl` int(2) NOT NULL,
  `registrar` tinyint(1) NOT NULL,
  `visualisar` tinyint(1) NOT NULL,
  `alterar` tinyint(1) NOT NULL,
  `deletar` tinyint(1) NOT NULL,
  UNIQUE KEY `id_tipo_pesoa` (`id_tipo_pessoa`,`id_acl`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `emprestimo_livro`
--

CREATE TABLE IF NOT EXISTS `emprestimo_livro` (
  `id_pessoa` int(2) NOT NULL,
  `id_livro` int(2) NOT NULL,
  `dt_devolucao` date NOT NULL,
  `dt_emprestimo` date NOT NULL,
  UNIQUE KEY `id_pessoa` (`id_pessoa`,`id_livro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `genero`
--

CREATE TABLE IF NOT EXISTS `genero` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `genero`
--

INSERT INTO `genero` (`id`, `nome`) VALUES
(1, 'terror'),
(2, 'romance'),
(3, 'suspence'),
(4, 'didatico');

-- --------------------------------------------------------

--
-- Estrutura da tabela `livro`
--

CREATE TABLE IF NOT EXISTS `livro` (
  `cod` int(64) NOT NULL,
  `titulo` varchar(150) NOT NULL,
  `autor` varchar(150) NOT NULL,
  `id_genero` int(2) NOT NULL,
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoa`
--

CREATE TABLE IF NOT EXISTS `pessoa` (
  `email` varchar(100) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `telefone` varchar(20) NOT NULL,
  `cpf` varchar(20) NOT NULL,
  `bairro` varchar(50) NOT NULL,
  `endereco` varchar(100) NOT NULL,
  `numEndereco` int(10) NOT NULL,
  `senha` varchar(64) NOT NULL,
  `id_tipo_pessoa` int(10) NOT NULL,
  PRIMARY KEY (`email`),
  UNIQUE KEY `id_tipo_pessoa` (`id_tipo_pessoa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_pessoa`
--

CREATE TABLE IF NOT EXISTS `tipo_pessoa` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `tipo_pessoa`
--

INSERT INTO `tipo_pessoa` (`id`, `nome`) VALUES
(1, 'ADMINISTRADOR'),
(2, 'BIBLIOTECARIO(A)'),
(3, 'USUARIO'),
(4, 'COORDENADOR(A)');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
