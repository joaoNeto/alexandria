package model;
public class AclPessoa {

    private int idTipoPessoa;
    private int idAcl;
    private Boolean registrar;
    private Boolean visualisar;
    private Boolean alterar;
    private Boolean deletar;

    public AclPessoa() {
    }

    public AclPessoa(int idTipoPessoa, int idAcl, Boolean registrar, Boolean visualisar, Boolean alterar, Boolean deletar) {
        this.idTipoPessoa = idTipoPessoa;
        this.idAcl = idAcl;
        this.registrar = registrar;
        this.visualisar = visualisar;
        this.alterar = alterar;
        this.deletar = deletar;
    }

    public int getIdTipoPessoa() {
        return idTipoPessoa;
    }

    public void setIdTipoPessoa(int idTipoPessoa) {
        this.idTipoPessoa = idTipoPessoa;
    }

    public int getIdAcl() {
        return idAcl;
    }

    public void setIdAcl(int idAcl) {
        this.idAcl = idAcl;
    }

    public Boolean getRegistrar() {
        return registrar;
    }

    public void setRegistrar(Boolean registrar) {
        this.registrar = registrar;
    }

    public Boolean getVisualisar() {
        return visualisar;
    }

    public void setVisualisar(Boolean visualisar) {
        this.visualisar = visualisar;
    }

    public Boolean getAlterar() {
        return alterar;
    }

    public void setAlterar(Boolean alterar) {
        this.alterar = alterar;
    }

    public Boolean getDeletar() {
        return deletar;
    }

    public void setDeletar(Boolean deletar) {
        this.deletar = deletar;
    }
    
    
    
}
