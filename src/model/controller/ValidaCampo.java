package model.controller;

public class ValidaCampo {

    private EnumTipoCampo tipoCampo;
    private String text;

    public ValidaCampo() {
    }

    public ValidaCampo(EnumTipoCampo tipoCampo, String text) {
        this.tipoCampo = tipoCampo;
        this.text = text;
    }

    public EnumTipoCampo getTipoCampo() {
        return tipoCampo;
    }

    public void setTipoCampo(EnumTipoCampo tipoCampo) {
        this.tipoCampo = tipoCampo;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
    
    
    
}
