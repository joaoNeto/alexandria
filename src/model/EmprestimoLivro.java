package model;

import java.util.Date;

public class EmprestimoLivro {

    private int idPessoa;
    private int idLivro;
    private Date dtDevolucao;
    private Date dtEmprestimo;

    public EmprestimoLivro() {
    }

    public EmprestimoLivro(int idPessoa, int idLivro, Date dtDevolucao, Date dtEmprestimo) {
        this.idPessoa = idPessoa;
        this.idLivro = idLivro;
        this.dtDevolucao = dtDevolucao;
        this.dtEmprestimo = dtEmprestimo;
    }

    public int getIdPessoa() {
        return idPessoa;
    }

    public void setIdPessoa(int idPessoa) {
        this.idPessoa = idPessoa;
    }

    public int getIdLivro() {
        return idLivro;
    }

    public void setIdLivro(int idLivro) {
        this.idLivro = idLivro;
    }

    public Date getDtDevolucao() {
        return dtDevolucao;
    }

    public void setDtDevolucao(Date dtDevolucao) {
        this.dtDevolucao = dtDevolucao;
    }

    public Date getDtEmprestimo() {
        return dtEmprestimo;
    }

    public void setDtEmprestimo(Date dtEmprestimo) {
        this.dtEmprestimo = dtEmprestimo;
    }
    
    
    
}
