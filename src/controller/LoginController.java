package controller;

import dao.PessoaDao;
import java.util.ArrayList;
import java.util.Map;
import model.Pessoa;
import model.controller.ResponseDataController;
import model.controller.EnumTipoCampo;
import model.controller.ValidaCampo;

public final class LoginController extends BaseController{

    @Override
    public Map<String, String> index() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public ResponseDataController logarUsuario(String email, String senha){
        Pessoa pessoa       = new Pessoa();
        PessoaDao pessoaDao = new PessoaDao();
        
        // validando os campos
        ArrayList<ValidaCampo> camposValidar = new ArrayList<ValidaCampo>();
        camposValidar.add(new ValidaCampo(EnumTipoCampo.email, email));
        camposValidar.add(new ValidaCampo(EnumTipoCampo.texto, senha));
        
        if(!super.validaCampos(camposValidar))
            return new ResponseDataController(false, "Voce inseriu campos  inválidos! ");
        
        // buscando a pessoa
        pessoa = pessoaDao.buscarUm(email, senha);
        
        if(pessoa == null)
            return new ResponseDataController(false, "Usuário não encontrado \n verifique se os campos foram digitados corretamente");

        // iniciando a sessao do usuario
        super.getUsuarioLogado();
        super.getUsuarioLogado().setEmail(pessoa.getEmail());
        super.getUsuarioLogado().setNome(pessoa.getNome());
        super.getUsuarioLogado().setBairro(pessoa.getBairro());
        super.getUsuarioLogado().setCpf(pessoa.getCpf());
        super.getUsuarioLogado().setTelefone(pessoa.getTelefone());
        super.getUsuarioLogado().setEndereco(pessoa.getEndereco());
        super.getUsuarioLogado().setNumEndereco(pessoa.getNumEndereco());
        super.getUsuarioLogado().setIdTipoPessoa(pessoa.getIdTipoPessoa());
        
        return new ResponseDataController(true, "Usuário logado com sucesso!");
    }
    
}
