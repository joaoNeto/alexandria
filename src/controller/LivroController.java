/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.GeneroDao;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import model.Genero;
import model.Livro;

/**
 *
 * @author aluno
 */
public class LivroController extends BaseController{

    Livro livro;
    
    public LivroController(Livro livro){
        this.livro = livro;
    }
    
    @Override
    public Map index() {
        Map retorno = new HashMap();
        GeneroDao generoDao = new GeneroDao();
        retorno.put("listaGenero", generoDao.buscarTodos());
        return retorno;
    }
    
}
