package model.controller;

public class ResponseDataController {

    private Boolean status;
    private String mensagem;

    public ResponseDataController(Boolean status, String mensagem) {
        this.status = status;
        this.mensagem = mensagem;
    }

    public Boolean getStatus() {
        return status;
    }

    public String getMensagem() {
        return mensagem;
    }
        
}
